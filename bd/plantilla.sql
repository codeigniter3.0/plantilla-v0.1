CREATE DATABASE  IF NOT EXISTS `plantilla` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `plantilla`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: plantilla
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_tipo_usuario`
--

DROP TABLE IF EXISTS `tbl_tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_usuario` (
  `cod_tipo_usuario` int(10) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) NOT NULL,
  `tipo_acceso` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_tipo_usuario`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_usuario`
--

LOCK TABLES `tbl_tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_usuario` DISABLE KEYS */;
INSERT INTO `tbl_tipo_usuario` VALUES (1,'ADMINISTRADOR','1'),(2,'REGISTRADOR','2'),(3,'SUPERVISOR','3');
/*!40000 ALTER TABLE `tbl_tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuario`
--

DROP TABLE IF EXISTS `tbl_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuario` (
  `cod_usuario` int(2) NOT NULL AUTO_INCREMENT,
  `nom_usuario` varchar(40) NOT NULL,
  `descripcion` varchar(40) NOT NULL,
  `clave` varchar(300) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `cod_tipo_usuario` int(10) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1,
  `clave_antigua` varchar(300) NOT NULL,
  `usuario_registro` varchar(50) NOT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cod_usuario`) USING BTREE,
  UNIQUE KEY `nom_usuario_UNIQUE` (`nom_usuario`),
  KEY `FK_tbl_tipousurio_usuario` (`cod_tipo_usuario`),
  CONSTRAINT `FK_tbl_usuario_1` FOREIGN KEY (`cod_tipo_usuario`) REFERENCES `tbl_tipo_usuario` (`cod_tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuario`
--

LOCK TABLES `tbl_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_usuario` DISABLE KEYS */;
INSERT INTO `tbl_usuario` VALUES (4,'DEYVI','ADMINISTRADOR','ywABOWOSHs7Rzfn3l_3V0P181ktpqD07eWJP5_4cCmE','2020-09-22 13:17:37',1,1,'','DEYVI-PC',NULL),(5,'JAIMITO','PRUEBA 1','jai123','2020-10-01 19:12:20',1,1,'','DEYVI-PC',NULL),(7,'JEAN','SUPERVISOR','6ukRwMCDre5VAQZ8A0q9JPBC5DY0VWi46IX_ClyMF3g','2020-11-24 16:22:24',3,1,'S3tCRbljJ8sS5cNtIO-0lGXSvpEXYfbTook811SsdyU','DESKTOP-FRMLHB1',NULL),(8,'TABO','REGISTRADOR','S3tCRbljJ8sS5cNtIO-0lGXSvpEXYfbTook811SsdyU','2020-11-24 16:25:01',2,1,'','DESKTOP-FRMLHB1',NULL);
/*!40000 ALTER TABLE `tbl_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'plantilla'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_insertar_usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_usuario`(

IN v_cod_tipo_usuario INT(10),
IN v_nom_usuario VARCHAR(40),
IN v_clave VARCHAR(300),
IN v_descripcion VARCHAR(40)

)
BEGIN
 start transaction;
        INSERT INTO tbl_usuario (cod_tipo_usuario,nom_usuario,descripcion,clave,usuario_registro,fecha_creacion)
        VALUES(v_cod_tipo_usuario,upper(v_nom_usuario),upper(v_descripcion),v_clave,@@hostname,now());
        commit;

	     select 1 as salida;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_listar_usuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_usuarios`()
BEGIN
SELECT nom_usuario as v1, descripcion as v2,fecha_creacion as v3, estado as v4

FROM tbl_usuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login`(in v_usuario varchar(40),in v_clave varchar(300) )
BEGIN
	select cod_usuario AS v7 ,nom_usuario as v2,tbt.descripcion as v3,tbu.descripcion as v4 from tbl_usuario as tbu inner join tbl_tipo_usuario as tbt 
    on tbu.cod_tipo_usuario=tbt.cod_tipo_usuario
    where nom_usuario=v_usuario and clave=v_clave;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-23 13:01:58
