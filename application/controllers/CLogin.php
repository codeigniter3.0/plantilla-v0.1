<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogin extends CI_Controller {

	function __construct(){
	 parent::__construct();
	 $this->load->helper("url");
     $this->load->helper("form");
	 $this->load->library('form_validation');
	 $this->load->model('MUsuario');
	 $this->load->library('encriptar'); 
	}

	public function index()
	{
		$data=array();
		$data['text1'] = $this->MUsuario->sp_listar_panellogin_1();
		$data['text2'] = $this->MUsuario->sp_listar_panellogin_2();
		
		$this->load->view('VLogin',$data);

	}

	public function login(){
        $respuesta = array();
        $respuesta['error'] = "";
        
        //$this->form_validation->set_rules('dni','DNI', 'trim|required|min_length[8]'array('required' => '%s es obligatorio','exact_lenght')
        
        $this->form_validation->set_rules('txtuser','Usuario','trim|required');
        $this->form_validation->set_rules('txtpass','Contraseña','trim|required');

        if ($this->form_validation->run() == FALSE)
                {
                        //error

                       $respuesta['error']= validation_errors();
                }
                else
                {
                        //acierto
                        $correo=$this->input->post("txtuser");
                        $clave=$this->security->xss_clean(strip_tags($this->input->post("txtpass")));
                        $data=array($correo,$this->encriptar->encode($clave));
                        $resultado=$this->MUsuario->login_mod($data);
                        if (isset($resultado)) {
                            $this->session->set_userdata('nom_usuario', $resultado->v2);
                            $this->session->set_userdata('coduser', $resultado->v7);
                            $this->session->set_userdata('tipo', $resultado->v3);
                            $this->session->set_userdata('descripcion', $resultado->v4);
                            
                        }else{
                            $respuesta['error']= "Datos incorrectos";
                        }
                        
                }
                header('Content-Type: application/x-json; charset=utf-8');
                            echo(json_encode($respuesta)); 
       
    }

	public function CerrarSesion()
	{
	  $this->session->sess_destroy();
	  header("Location: ".base_url());
	}


}
