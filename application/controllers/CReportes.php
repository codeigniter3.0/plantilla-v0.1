<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReportes extends CI_Controller {

	function __construct(){
	 parent::__construct();
	 $this->load->helper("url");
	 $this->load->library('form_validation');
	 $this->load->model('MUsuario');
	}


	public function index()
	{
		$this->load->view('VReporte');
	}
public function Reporte()
	{
		$this->load->view('VReporte');
	}
public function Estadisticas()
	{
		$this->load->view('VEstadisticas');
	}

}
