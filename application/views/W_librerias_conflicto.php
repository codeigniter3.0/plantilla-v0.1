<script src="<?= base_url()?>template/html/js/vendor-core.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-countdown.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-tables.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-forms.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-carousel-slick.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-player.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-charts-flot.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-nestable.min.js"></script>

    <script src="<?= base_url()?>template/html/js/module-essentials.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-material.js"></script>
    <script src="<?= base_url()?>template/html/js/module-layout.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-sidebar.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-carousel-slick.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-player.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-messages.min.js"></script>
    <!-- <script src="<?= base_url()?>template/html/js/module-maps-google.min.js"></script> -->
    <script src="<?= base_url()?>template/html/js/module-charts-flot.min.js"></script>

    <script src="<?= base_url()?>template/html/js/theme-core.min.js"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?= base_url()?>template/lib/bootstrap-notify/bootstrap-notify.js"></script>