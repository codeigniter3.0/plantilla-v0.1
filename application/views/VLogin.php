<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('tipo') == ""){
?>
<!DOCTYPE html>
<html class="hide-sidebar ls-bottom-footer" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SISTEMA XXXXX</title>

    <link href="<?= base_url()?>template/html/css/vendor.min.css" rel="stylesheet">

    <link href="<?= base_url()?>template/html/css/theme-core.css" rel="stylesheet">

    <link href="<?= base_url()?>template/html/css/module-essentials.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-material.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-layout.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-sidebar.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-sidebar-skins.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-navbar.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-messages.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-carousel-slick.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-charts.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-maps.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-alerts.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-background.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-buttons.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-text.min.css" rel="stylesheet" />
    <script src="<?=base_url()?>js/jquery-3.1.0.js"></script>
    <script src="<?= base_url()?>js/jsLogin.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
        WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    
</head>
<body class="login">

   

    
        
        <div class="container-fluid contentlogin" style="">
            
            <div class="col-md-4 col-lg-4 col-sm-5 ">
                <div class="panel panel-default text-center paper-shadow" data-z="0.5">
                    <h1 class="text-display-1 text-center margin-bottom-none">XXXXXXXX</h1>                    
                    <img src="<?= base_url()?>template/html/images/learning-logo.png" class="img-circle2 width-100">
                    <br>
                    <br>
                    <div class="panel-body">
                        <form class="form-horizontal" id="singIn">
                            <div class="form-group">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-7">
                                    <div class="form-control-material">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input id="txtuser" name="txtuser" type="text" class="form-control" placeholder="Ingrese su Usuario">
                                            <label for="txtuser" style="text-align: initial;">Usuario</label>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-7">
                                    <div class="form-control-material">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-fw fa-lock"></i></span>
                                            <input id="txtpass" name="txtpass" type="password" class="form-control" placeholder="Ingrese su Clave">
                                            <label for="txtpass" style="text-align: initial;">Clave</label>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" >Ingresar <i class="fa fa-fw fa-unlock-alt"></i></button>
                            </div>  
                            <div class="alert alert-danger"  id="error" >
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                              <p id="mensaje_error"></p>
                            </div> 
                            <p class="incorrecto">
                                <?php echo validation_errors();?>
                                <?php echo $this->session->flashdata('mensaje');
                                ?>
                                <?php echo $this->session->flashdata('alert');
                                ?>
                            </p>
                            <br>
                            <a href="#" class="forgot-password">Olvidaste la contraseña?</a>
                            <a href="sign-up.html" class="link-text-color">Crear nueva cuenta</a>  
                        </form>                       
                    </div>                    
                </div>

            </div>
            
        </div>


        <!-- Footer -->
        <footer class="footer">
            <strong>PLANTILLA SISTEMA</strong> V1.0 - TABO &copy; Copyright 2021 - Todos los derechos Reservados
        </footer>
        <!-- // Footer -->

        

    <!-- Inline Script for colors and config objects; used by various external scripts; -->
    <script>
        var colors = {
            "danger-color": "#e74c3c",
            "success-color": "#81b53e",
            "warning-color": "#f0ad4e",
            "inverse-color": "#2c3e50",
            "info-color": "#2d7cb5",
            "default-color": "#6e7882",
            "default-light-color": "#cfd9db",
            "purple-color": "#9D8AC7",
            "mustard-color": "#d4d171",
            "lightred-color": "#e15258",
            "body-bg": "#f6f6f6"
        };
        var config = {
            theme: "html",
            skins: {
                "default": {
                    "primary-color": "#42a5f5"
                }
            }
        };

    
            ruta='<?=base_url()?>';
            $(document).ready(

              function(){
                dosave();
              }

              );

         
    </script>
    <!-- Separate Vendor Script Bundles -->
    <script src="<?= base_url()?>template/html/js/vendor-core.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-countdown.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-tables.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-forms.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-carousel-slick.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-player.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-charts-flot.min.js"></script>
    <script src="<?= base_url()?>template/html/js/vendor-nestable.min.js"></script>
    <!-- <script src="<?= base_url()?>template/html/js/vendor-angular.min.js"></script> -->
    <!-- Compressed Vendor Scripts Bundle
    Includes all of the 3rd party JavaScript libraries above.
    The bundle was generated using modern frontend development tools that are provided with the package
    To learn more about the development process, please refer to the documentation.
    Do not use it simultaneously with the separate bundles above. -->
    <!-- <script src="<?= base_url()?>template/html/js/vendor-bundle-all.min.js"></script> -->
    <!-- Compressed App Scripts Bundle
    Includes Custom Application JavaScript used for the current theme/module;
    Do not use it simultaneously with the standalone modules below. -->
    <!-- <script src="<?= base_url()?>template/html/js/module-bundle-main.min.js"></script> -->
    <!-- Standalone Modules
    As a convenience, we provide the entire UI framework broke down in separate modules
    Some of the standalone modules may have not been used with the current theme/module
    but ALL the modules are 100% compatible -->
    <script src="<?= base_url()?>template/html/js/module-essentials.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-material.js"></script>
    <script src="<?= base_url()?>template/html/js/module-layout.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-sidebar.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-carousel-slick.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-player.min.js"></script>
    <script src="<?= base_url()?>template/html/js/module-messages.min.js"></script>
    <!-- <script src="<?= base_url()?>template/html/js/module-maps-google.min.js"></script>-->
    <script src="<?= base_url()?>template/html/js/module-charts-flot.min.js"></script>
    <!-- [html] Core Theme Script:
        Includes the custom JavaScript for this theme/module;
        The file has to be loaded in addition to the UI modules above;
        module-bundle-main.js already includes theme-core.js so this should be loaded
        ONLY when using the standalone modules; -->
        <script src="<?= base_url()?>template/html/js/theme-core.min.js"></script>
        
    </body>
    </html>

<?php }else{

redirect(base_url().'CPanel');
}?>
