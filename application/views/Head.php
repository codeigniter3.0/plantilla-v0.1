<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Derechos Reservados D">
    <title>SISTEMA POSTGRADO</title>
    <!--    estilos de terceros como el boostrap  -->
    <link href="<?= base_url()?>template/html/css/vendor.min.css" rel="stylesheet">
    <!-- incluye todos los estilos personalizados necesarios para el tema actual no inbluye modulos independientes. -->
    <!-- <link href="<?= base_url()?>template/html/css/theme.bundle.min.css" rel="stylesheet"> -->
    <!-- CORE separar los modulos de estilos CSS -->
    <link href="<?= base_url()?>template/html/css/theme-core.min.css" rel="stylesheet">
    <!-- todos los modulos son 100% compatibles -->
    <link href="<?= base_url()?>template/html/css/module-essentials.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-material.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-layout.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-sidebar.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-sidebar-skins.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-navbar.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-messages.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-carousel-slick.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-charts.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-maps.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-alerts.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-background.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-buttons.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>template/html/css/module-colors-text.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
