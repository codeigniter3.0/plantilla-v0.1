<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccessUsers extends CI_Hooks{

private $ci;
public function __construct()
{  
  $this->ci=& get_instance();
  !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
  !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
}

public function verificarAcceso(){
$class = strtoupper($this->ci->router->class); 
$method = strtoupper($this->ci->router->method);
$nocontrolados = array('CLOGIN');
/* No solicitan acceso */
$session = $this->ci->session->userdata('nom_usuario');  
if (!in_array($class, $nocontrolados))
  if(empty($session)) {
   redirect(base_url().'#');
  }
  else{

  }
}

public function checkAccess(){
  $class = strtoupper($this->ci->router->class); //Class llamada
  $method = strtoupper($this->ci->router->method); //metodo llamado
  $nocontrolados = array('CLOGIN');  
  $session = $this->ci->session->userdata('nom_usuario');  
  $paginas = $this->ci->session->userdata('paginas');  
  if (!in_array($class, $nocontrolados))
   if(empty($session)) {
     redirect(base_url().'#');
      
   }else if (!in_array($class.$method, $paginas)) {
      redirect(base_url().'ErrorAcceso');
   }

}
}

 /* if(!empty($session) && !in_array($class, $this->allowed_controllers))
  {
    if(in_array($method, $this->allowed_methods))
    {
      redirect('Perfil');
    }
  }
  if(!empty($session) && in_array($class, $this->allowed_controllers))
  {
    if(!in_array($method, $this->allowed_methods))
    {
      redirect('Perfil');
    }
  }*/


