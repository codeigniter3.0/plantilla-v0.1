<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MMatricula extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->helper("url");
    $this->load->model('MProcedimientos');
	}

   	
    public function mlistar_cbprograma() {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbprograma',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

    public function mlistar_cbmencion() {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbmencion',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

     public function mlistar_cbmenciondep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmenciondep',$data);         
        $this->db->close(); 
        return $resultado;
    }

    public function mlistar_cbalumnodep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmencion_alumnodep',$data);         
        $this->db->close(); 
        return $resultado;
    }
         
    public function mlistar_cbalumno_semdep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmencion_sem_alumnodep',$data);    
        $this->db->close(); 
        return $resultado;
    }
    
    public function mlistar_cbmencion_semestre($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmension_semestredep',$data);      
        $this->db->close(); 
        return $resultado;
    }

     public function mlistar_cbalumno_curso($data) {
      /* viene de inscripcion pero fue borrado, ver si viene de otro lado*/
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmenciondep',$data);         
        $this->db->close(); 
        return $resultado;
    }

     public function mlistar_cbmencion_cursodep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmencion_cursodep',$data);         
        $this->db->close(); 
        return $resultado;
    }

     public function mlistar_cbmencion_plandep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmencion_plandep',$data);         
        $this->db->close(); 
        return $resultado;
    }

        public function mlistar_cbalumno()
    {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbalumno',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

    public function mlistar_cbcursos() {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbcursos',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

    public function mlistar_cbprofesor() {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbprofesor',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'].' '.$obj['v3'].' '.$obj['v4'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

    public function mlistar_cbsede() {
        $this->load->database();
        $res=array();
        $res= $this->MProcedimientos->get_procedure('sp_listar_cbsede',null);
        mysqli_next_result($this->db->conn_id);
        if(!empty($res)){
            $arreglo=array();
            $arreglo[""]="";
            foreach($res as $obj){
                $arreglo[$obj['v1']]=$obj['v2'];
            }
            return $arreglo;                
        }else{            
            return false;
        }
    }

    public function sp_RegistrarMatricula($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_matricula',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_listar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_profesor()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }

     public function sp_Buscar($data)
    {
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_buscar_matricula',$data);         
        $this->db->close(); 
        return $resultado;
    }

    public function mbuscar_matricula_update($data)
    {
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_buscar_matricula_update',$data);         
        $this->db->close(); 
        return $resultado;
    }
   
}

?>