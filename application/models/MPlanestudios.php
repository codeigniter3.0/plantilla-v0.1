<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPlanestudios extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->helper("url");
    $this->load->model('MProcedimientos');
	}

 public function cblistar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_cbplan()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }

 public function sp_RegistrarPlan($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_plan',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

public function sp_listar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_profesor()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }
     
 public function sp_Buscar($data)
    {
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_buscar_plan',$data);         
        $this->db->close(); 
        return $resultado;
    }

}

?>