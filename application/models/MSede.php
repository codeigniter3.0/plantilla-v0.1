<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MSede extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->helper("url");
    $this->load->model('MProcedimientos');
	}

 public function cblistar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_cbsede()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }
       
 
}

?>