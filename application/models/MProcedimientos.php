<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MProcedimientos extends CI_Model {
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();		
	}
	
	function safe_escape($data){
		$parametros="";
		
		if(count($data) <= 0){
			return "";
		}
		for($i = 0; $i < count($data); $i++){
			//el punto es para concatenar al igual que java con +=
			$parametros.=$this->db->escape($data[$i]);
			if($i < ((count($data) -1))){
				$parametros.=",";
			}
		}	
		return $parametros;	
	}
	
	public function get_procedure($proc, $param){
		if($param == NULL){
			$sql = $this->db->query("CALL $proc()");			
		}
		else{
			$param = $this->safe_escape($param);
			$sql = $this->db->query("CALL $proc($param)");			
		}
		$rows = array();
		foreach($sql->result_array() as $fila){
			//indica un arreglo dinamico tipo arraylist que no tiene fin, el unico fin seria el for
			$rows[]= $fila;
			
		}
		return $rows;
	}
	
	
}



?>