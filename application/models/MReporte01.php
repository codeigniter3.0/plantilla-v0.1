<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MReporte extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->model('MProcedimientos');	
	}
	
	
    public function sp_Reportes($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_reporte_01',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

?>