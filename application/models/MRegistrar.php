<?php  
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class MRegistrar extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->model('MProcedimientos');	
	}
//**********************Registrar Area  *************************************
	public function registrarArea($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_area',$par);
		return $resultado[0];
	}
//*************************************************************************************
//**********************Registrar Cargo ******************************************+****
	public function registrarCargo($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_cargo',$par);
		return $resultado[0];
	}
//**************************************************************************************
//**********************Registrar Tipo de Documento *********************************
	public function registrarTipoDocumeto($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_TipoDocu',$par);
		return $resultado[0];
	}
//**************************************************************************************
//**********************Registrar  Tipo Tramite *******************************************
	public function registrarTipoTramite($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_TipoTramite',$par);
		return $resultado[0];
	}
//*************************************************************************************************
//**********************Registrar  Usuario *******************************************
	public function registrarUsuario($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_usuario',$par);
		return $resultado[0];
	}
//*************************************************************************************************
//**********************Registrar  Prioridad *******************************************
	public function registrarPrioridad($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_prioridad',$par);
		return $resultado[0];
	}
//**************************************************************************************	
//**********************Registrar Asignacion de Actividad *******************************************
	public function registrarActividad($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_reg_asignacion_actividad',$par);
		return $resultado[0];
	}
//**********************************************************************************************
//**********************Registrar  Detalle Asignacion*******************************************
	public function registrarDetalleAsignacion($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_detalle_asignacion',$par);
		return $resultado[0];
	}

//**********************Registrar Informe de Usuario Externo *******************************************
    public function registrarInformeUsuExter($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_informeusuExter',$par);
		return $resultado[0];
	}
//**********************************************************************************************
//**********************Registrar Movimiento *******************************************
	public function registrarMovimiento($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_movimiento',$par);
		return $resultado[0];
	}
//**************************************************************************************
//**********************Registrar Detalle de Tramite *******************************************
	public function registrarDetalleTramite($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_detalleTramite',$par);
		return $resultado[0];
	}
//**************************************************************************************
//**********************Registrar Documentos*******************************************
	public function registrarDocumento($par){
		$this->load->database();
		$resultado = $this->MProcedimientos->get_procedure('sp_registrar_documento',$par);
		return $resultado[0];
	}
//*************************************************************************************
 }
 ?>