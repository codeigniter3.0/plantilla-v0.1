<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MInscripcion extends CI_Model{    
    function __construct(){ 
        //parent = super de java, hace la llamada al constructor padre
        parent::__construct();  
        $this->load->helper("url");
    $this->load->model('MProcedimientos');
    }

    public function sp_RegistrarIncripcion($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_inscripcion_curso',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

   public function sp_listar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_profesor()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }

     public function sp_Buscar_alumnoxcurso($data)
    {
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_buscar_inscrip_alumnoxcursos',$data);        
        $this->db->close(); 
        return $resultado;
    }
    
     public function sp_Buscar_cursoxalumno($data)
    {
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_buscar_inscrip_cursosxalumno',$data);        
        $this->db->close(); 
        return $resultado;
    }

     public function mlistar_cbMencion_Sem_Cursosdep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbmensionysemestre_curso',$data);    
        $this->db->close(); 
        return $resultado;
    }

      public function mlistar_cbSem_Alumno_Cursosdep($data) {
      
        $this->load->database();  
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_cbsemestreyalumno_curso',$data);    
        $this->db->close(); 
        return $resultado;
    }
    
}

?>