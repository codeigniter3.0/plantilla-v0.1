<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MPrograma extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->helper("url");
    $this->load->model('MProcedimientos');
	}
 public function sp_listar()
    {
        $this->load->database();  
        $qry = "CALL sp_listar_alumno()";
         $result = $this->db->query($qry);
        $this->db->close(); 
        return $result->result_array();     
    }
    
	 public function sp_RegistraAlumno($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_alumno',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }


}

?>