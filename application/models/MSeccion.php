<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MSeccion extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->helper("url");
    $this->load->model('MProcedimientos');
	}

    public function sp_RegistraUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_Usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }


    public function sp_ModificarUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_modificar_usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_EliminarUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_eliminar_usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_HabilitarUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_habilitar_usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_InhabilitarUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_inhabilitar_usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
   
    public function sp_CambiarClaveUsuario($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_cambiarclave_usuario',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_RegistraSesion($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_sesion',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];       
    }
	
	public function obtenerUsuario($usuario,$password)
  {
      

        $this->db->select('cod_usuario,cod_tipo_usuario,cod_nivel_usuario,Clave,Usuario,estado');
        $this->db->from('tbl_usuario');
        $this->db->where('usuario', $usuario);
        $this->db->where('clave', $password);
        $this->db->where('estado', 1);

        $consulta = $this->db->get();
        $resultado = $consulta->row();

        if($resultado != null) //si hay filas
        {
          //retornar el resultado de la consulta
          return $resultado;
        }
        else //si no hay resultados distintos de cero
        {
          //retorne null
          return null;
        }


      }

         public function getCbonivel() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_nivelusuario_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }

    public function getCbotusua() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_tusuario_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }

    public function listar_tipo_usuario($cod_usuario) {

    $this->load->database();

      $res=array();

      //$res= $this->MProcedimientos->get_procedure('sp_listar_tipo_usuario',$cod_usuario);
      $res= $this->db->query("call sp_listar_tipo_usuario(".$cod_usuario.")");
      mysqli_next_result($this->db->conn_id); 
      if(!empty($res)){
        $arreglo;
        foreach($res->result_array() as $obj){
          $arreglo=$obj['tipusu'];
        }
        return $arreglo;
        
      }else{
        
        return false;
      }

       
    }




        public function sp_Sesion_usuario_ultimo($usu) {

    $this->load->database();

      $res=array();

      //$res= $this->MProcedimientos->get_procedure('sp_listar_area_usuario',$usuario);
       $res= $this->db->query("call sp_ultima_sesion(".$usu.")");
      mysqli_next_result($this->db->conn_id); 
      if(!empty($res)){
        $arreglo="";
        foreach($res->result_array() as $obj){
          $arreglo=$obj['ses'];
        }
        return $arreglo;
        
      }else{
        
        return false;
      }

       
    }


    public function listarUsuario_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_usuario_table',null);
        mysqli_next_result($this->db->conn_id);
        return $resultado;
    }

     public function listarUsuarioinhabilitados_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_usuario_inhabilitado_table',null);
        mysqli_next_result($this->db->conn_id);
        return $resultado;
    }


    public function obtenerclaveactual($usuc) {

    $this->load->database();

      $res=array();

      //$res= $this->MProcedimientos->get_procedure('sp_listar_area_usuario',$usuario);
      $res= $this->db->query("call sp_clave_actual(".$usuc.")");
      mysqli_next_result($this->db->conn_id); 
      if(!empty($res)){
        $arreglo;
        foreach($res->result_array() as $obj){
          $arreglo=$obj['clave'];
        }
        return $arreglo;
        
      }else{
        
        return false;
      }

       
    }
   

    
    public function listarUltimas_sesiones_table($usu)
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_utimas_sesiones_table',$usu);
        mysqli_next_result($this->db->conn_id); 
        
        return $resultado;
    }
   
	
}

?>