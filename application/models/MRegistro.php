<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MRegistro extends CI_Model{	
	function __construct(){	
		//parent = super de java, hace la llamada al constructor padre
		parent::__construct();	
		$this->load->model('MProcedimientos');	
	}
	
	
	public function sp_RegistraComprobante($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_comprobante',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_RegistraDatoscomplement($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_datocomplement',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_RegistraDistribuidora($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_distribuidora',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_RegistraProveedor($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_Proveedor',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_RegistraArchivosadj($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_insertar_nuevo_archivo_adjunto',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_ModificarArchivosadj($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_modificar_archivo_adjunto',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_ModificarProveedor($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_modificar_proveedor',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_ModificaComprobante($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_modificar_comprobante',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_ModificarDistribuidora($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_modificar_distribuidora',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

    public function sp_AprobarComprobante($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_aprobar_comprobante',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }

      public function sp_EditarProveedor($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_Editar_proveedor',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
    public function sp_EditarDistribuidora($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_Editar_distribuidora',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }
     public function sp_EliminarPdf($data){
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_eliminar_pdf',$data);
        mysqli_next_result($this->db->conn_id);
        return $resultado[0];
    }


	public function getdistribuidora() {

		$this->load->database();

			$res=array();

			$res= $this->MProcedimientos->get_procedure('sp_listar_distrib_combo',null);
			mysqli_next_result($this->db->conn_id);
			if(!empty($res)){
				$arreglo=array();
				$arreglo[""]="Seleccione...";
				foreach($res as $obj){
					$arreglo[$obj['cd']]=$obj['nom'];
				}
				return $arreglo;
				
			}else{
				
				return false;
			}
    }
    public function gettipocomprobante() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_tcomprob_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }
        public function getclasecomprobante() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_clascomprob_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }
     public function gettipomoneda() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_tmoneda_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }
      public function getproveedor() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_proveedor_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }
      public function getestado() {

        $this->load->database();

            $res=array();

            $res= $this->MProcedimientos->get_procedure('sp_listar_estado_combo',null);
            mysqli_next_result($this->db->conn_id);
            if(!empty($res)){
                $arreglo=array();
                $arreglo[""]="Seleccione...";
                foreach($res as $obj){
                    $arreglo[$obj['cd']]=$obj['nom'];
                }
                return $arreglo;
                
            }else{
                
                return false;
            }
    }
 
   
 
   

    public function listarComprobantes_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_comprobantes_table',null);
        
        return $resultado;
    }
    

    public function listarDatoscomplement_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_datoscomplement_table',null);
        
        return $resultado;
    }
    public function listaRAcomprobante_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_acomprobante_table',null);
        
        return $resultado;
    }
    public function listarDistribuidora_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_distribuidora_table',null);
        
        return $resultado;
    }
    public function listarProveedor_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_proveedor_table',null);
        
        return $resultado;
    }

    public function listarAreas_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_areas_table',null);
        
        return $resultado;
    }

    public function listarTdocs_table()
    {
        $this->load->database();
        $resultado = $this->MProcedimientos->get_procedure('sp_listar_tdocs_table',null);
        
        return $resultado;
    }

    public function listarDocumentos_Recepcionados($codarea)
    {
        $this->load->database();
        //$resultado = $this->MProcedimientos->get_procedure('sp_listar_documento_entramite',$nexped);
        $resultado= $this->db->query("call sp_listar_documento_recepcionados(".$codarea.")");
        mysqli_next_result($this->db->conn_id);
        return $resultado->result_array();
    }


   

    
 
	
}

?>